//
//  HomeScreenViewController.swift
//  DbDemoExampleSwift
//
//  Created by TheAppGuruz-New-6 on 11/08/15.
//  Copyright (c) 2015 TheAppGuruz-New-6. All rights reserved.
//

import UIKit

class HomeScreenViewController: UIViewController , UITableViewDataSource,UITableViewDelegate {
    
    var marrStudentData : NSMutableArray!
    var totaPages:NSString = "0";
    var currentPage:Int = 1;
    var arrPeopleInfo:NSArray = [];
    var recordsToUpdate = 0;
    
    @IBOutlet weak var tbStudentData: UITableView!
    @IBOutlet weak var titulo: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getStudentData();
        self.getData();
        
        
    }
    
    func getData(){
        if(self.currentPage == self.totaPages.integerValue){
            return;
        }
        let qualityOfServiceClass = QOS_CLASS_BACKGROUND
        let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
        dispatch_async(backgroundQueue, {
            // Do any additional setup after loading the view.
            let url = "https://api.themoviedb.org/3/movie/popular?api_key=ff17be5f33ecef90e365ed15bab2eacc&language=en-US&page=\(self.currentPage)"
            do{
                
                if let weatherData = NSData(contentsOfURL: NSURL(string: url)!) {
                    if let jsonObject : AnyObject? = try NSJSONSerialization.JSONObjectWithData(weatherData, options: NSJSONReadingOptions.MutableContainers){
                        if let statusesArray = jsonObject as? NSDictionary{
                            if(self.totaPages.isEqualToString("\(statusesArray["total_pages"])")){
                                self.totaPages = statusesArray["total_pages"] as! String;
                            }
                            if((statusesArray["results"]) != nil){
                                for var item in statusesArray["results"] as! [NSDictionary] {
                                    print("row -> \(item)");
                                    let id:Int = Int(item["id"]!.stringValue)!;
                                    if(ModelManager.getInstance().getMovie(id).count > 0){
                                        print("find")
                                    }else{
                                        let movie: Movies = Movies()
                                        //id, title, popularity, poster_path, overview, release_date, category
                                        movie.Id = id
                                        movie.Title = item["title"] as! String
                                        movie.Popularity = (item["popularity"]?.stringValue)!
                                        if let path = item["poster_path"] as? String{
                                            movie.Poster_Path = path;
                                        }else{
                                            movie.Poster_Path = "";
                                        }
                                        
                                        movie.Overview = item["overview"] as! String
                                        movie.Release_Date = item["release_date"] as! String
                                        movie.Category = "popular"
                                        let isInserted = ModelManager.getInstance().addStudentData(movie)
                                        if isInserted {
                                            //Util.invokeAlertMethod("", strBody: "Record Inserted successfully.", delegate: nil)
                                            self.recordsToUpdate++;
                                            
                                        } else {
                                            Util.invokeAlertMethod("", strBody: "Error in inserting record.", delegate: nil)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                
            }catch let error as NSError{
                print("error -> \(error)")
            }
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.currentPage++;
                //self.tbStudentData.reloadData();
                self.titulo.text = "\(self.recordsToUpdate) peliculas agregadas"
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(3 * NSEC_PER_SEC) ), dispatch_get_main_queue(), {
                    self.getData();
                });
                
            })
        })
    }
    
    override func viewWillAppear(animated: Bool) {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Other methods
    
    func getStudentData()
    {
        marrStudentData = NSMutableArray()
        marrStudentData = ModelManager.getInstance().getAllMovies()
        tbStudentData.reloadData()
    }
    
    //MARK: UITableView delegate methods
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return marrStudentData.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:StudentCell = tableView.dequeueReusableCellWithIdentifier("cell") as! StudentCell
        let movie:Movies = marrStudentData.objectAtIndex(indexPath.row) as! Movies
        cell.lblContent.text = "\(movie.Title)"
        cell.btnDelete.tag = indexPath.row
        //cell.btnEdit.tag = indexPath.row
        return cell
    }
    
    //MARK: UIButton Action methods

    @IBAction func btnDeleteClicked(sender: AnyObject) {
        self.performSegueWithIdentifier("editSegue", sender: sender)
        /*let btnDelete : UIButton = sender as! UIButton
        let selectedIndex : Int = btnDelete.tag
        let movie: Movies = marrStudentData.objectAtIndex(selectedIndex) as! Movies
        let isDeleted = ModelManager.getInstance().deleteStudentData(movie)
        if isDeleted {
            Util.invokeAlertMethod("", strBody: "Record deleted successfully.", delegate: nil)
        } else {
            Util.invokeAlertMethod("", strBody: "Error in deleting record.", delegate: nil)
        }
        self.getStudentData()*/
    }

    @IBAction func btnEditClicked(sender: AnyObject)
    {
        self.performSegueWithIdentifier("editSegue", sender: sender)
    }
    
    @IBAction func reload(sender: AnyObject)
    {
        self.recordsToUpdate = 0;
        self.tbStudentData.reloadData();
    }
    
    //MARK: Navigation methods

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "editSegue")
        {
            let btnEdit : UIButton = sender as! UIButton
            let selectedIndex : Int = btnEdit.tag
            let viewController : InsertRecordViewController = segue.destinationViewController as! InsertRecordViewController
            viewController.isEdit = true
            viewController.movie = marrStudentData.objectAtIndex(selectedIndex) as! Movies
        }
    }

}
