//
//  InsertRecordViewController.swift
//  DbDemoExampleSwift
//
//  Created by TheAppGuruz-New-6 on 11/08/15.
//  Copyright (c) 2015 TheAppGuruz-New-6. All rights reserved.
//

import UIKit

class InsertRecordViewController: UIViewController {

    @IBOutlet weak var Image: UIImageView!
    @IBOutlet weak var MyTitle: UILabel!
    @IBOutlet weak var MyPop: UILabel!
    @IBOutlet weak var MyText: UITextView!
    
    var isEdit : Bool = false
    var movie : Movies!
    var id: Int = 0;
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.MyTitle.text = movie.Title
        self.MyPop.text = "Popularity: \(movie.Popularity)"
        self.MyText.text = movie.Overview
        self.MyText.setContentOffset(CGPointZero, animated: false);
        // Do any additional setup after loading the view.
        if let url = NSURL(string: "https://image.tmdb.org/t/p/w185/\(movie.Poster_Path)") {
            if let data = NSData(contentsOfURL: url) {
                self.Image.image = UIImage(data: data)
            }        
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: UIButton Action methods
    
    @IBAction func btnBackClicked(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func btnSaveClicked(sender: AnyObject)
    {
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
