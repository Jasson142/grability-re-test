//
//  StudentInfo.swift
//  DataBaseDemo
//
//  Created by Krupa-iMac on 05/08/14.
//  Copyright (c) 2014 TheAppGuruz. All rights reserved.
//

import UIKit

class Movies: NSObject {
    
    var Id: Int = Int()
    var Title: String = String()
    var Popularity: String = String()
    var Poster_Path: String = String()
    var Overview: String = String()
    var Release_Date: String = String()
    var Category: String = String()
   
}
