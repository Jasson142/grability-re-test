//
//  ModelManager.swift
//  DataBaseDemo
//
//  Created by Krupa-iMac on 05/08/14.
//  Copyright (c) 2014 TheAppGuruz. All rights reserved.
//

import UIKit

let sharedInstance = ModelManager()

class ModelManager: NSObject {
    
    var database: FMDatabase? = nil

    class func getInstance() -> ModelManager
    {
        if(sharedInstance.database == nil)
        {
            sharedInstance.database = FMDatabase(path: Util.getPath("db.sql"))
        }
        return sharedInstance
    }
    
    func addStudentData(movie: Movies) -> Bool {
        sharedInstance.database!.open()
        let isInserted = sharedInstance.database!.executeUpdate("INSERT INTO movies (id, title, popularity, poster_path, overview, release_date, category) VALUES (?, ?, ?, ?, ?, ?, ?)", withArgumentsInArray: [movie.Id, movie.Title, movie.Popularity, movie.Poster_Path, movie.Overview, movie.Release_Date, movie.Category])
        sharedInstance.database!.close()
        return isInserted
    }
   
    func updateStudentData(movie: Movies) -> Bool {
        sharedInstance.database!.open()
        let isUpdated = sharedInstance.database!.executeUpdate("UPDATE movies SET title=?, popularity=?, poster_path=?, overview=?, release_date=?, category=? WHERE id=?", withArgumentsInArray: [movie.Title, movie.Popularity, movie.Poster_Path, movie.Overview, movie.Release_Date, movie.Category, movie.Id])
        sharedInstance.database!.close()
        return isUpdated
    }
    
    func deleteStudentData(movies: Movies) -> Bool {
        sharedInstance.database!.open()
        let isDeleted = sharedInstance.database!.executeUpdate("DELETE FROM movies WHERE id=?", withArgumentsInArray: [movies.Id])
        sharedInstance.database!.close()
        return isDeleted
    }
    
    func getMovie(id: Int) -> NSMutableArray {
        sharedInstance.database!.open()
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("SELECT * FROM movies WHERE id = ? ORDER BY popularity DESC", withArgumentsInArray: [id])
        let marrStudentInfo : NSMutableArray = NSMutableArray()
        if (resultSet != nil) {
            while resultSet.next() {
                let movie : Movies = Movies()
                movie.Id = Int(resultSet.stringForColumn("id"))!
                movie.Title = resultSet.stringForColumn("Title")
                movie.Popularity = resultSet.stringForColumn("Popularity")
                movie.Poster_Path = resultSet.stringForColumn("Poster_Path")
                movie.Overview = resultSet.stringForColumn("Overview")
                movie.Release_Date = resultSet.stringForColumn("Release_Date")
                movie.Category = resultSet.stringForColumn("Category")
                marrStudentInfo.addObject(movie)
            }
        }
        sharedInstance.database!.close()
        return marrStudentInfo
    }

    func getAllMovies() -> NSMutableArray {
        sharedInstance.database!.open()
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("SELECT * FROM movies", withArgumentsInArray: nil)
        let marrStudentInfo : NSMutableArray = NSMutableArray()
        if (resultSet != nil) {
            while resultSet.next() {
                let movie : Movies = Movies()
                movie.Id = Int(resultSet.stringForColumn("id"))!
                movie.Title = resultSet.stringForColumn("Title")
                movie.Popularity = resultSet.stringForColumn("Popularity")
                movie.Poster_Path = resultSet.stringForColumn("Poster_Path")
                movie.Overview = resultSet.stringForColumn("Overview")
                movie.Release_Date = resultSet.stringForColumn("Release_Date")
                movie.Category = resultSet.stringForColumn("Category")
                marrStudentInfo.addObject(movie)
            }
        }
        sharedInstance.database!.close()
        return marrStudentInfo
    }
}
